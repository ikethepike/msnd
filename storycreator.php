<!DOCTYPE html>
<html>
<head>
	<title>Storycreator</title>

	<style>
	*{
		-webkit-box-sizing: border-box;
		box-sizing: border-box;

	}
		#story-creator{
			width:100%;
			max-width:45em;
			margin:auto;
			position: relative;
		}
		.field{
			margin:1em 0;
			clear:both;
		}
		input, textarea, select{
			resize:none;
			width:100%;
			border:1px solid #252525;
			min-height:3.5em;
			border-bottom:2px solid #252525;
		}
		label{
			display:block;
			padding:.35em 0;
			font-weight:bold;
		}
		.option{
			clear:both;
			position: relative;
			width:100%;
			min-height:4em;
		}
		.option input{
			width:50%;
			display:block;

			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
			float:left;
		}
		.option:last-child{
			margin-bottom:2em;
		}
		#message{
			position: absolute;
			left:0;
			top:-4em;
			z-index:100;
			width:100%;
			background: green;
			color: #fff;
			text-align: center;
			padding:.5em 0;
			-webkit-animation: slideOut 2s 1;
			-moz-animation: slideOut 2s 1;
			animation: slideOut 2s 1;
		}
		.specials .field{
			width:50%;
			float:left;
			margin-bottom:.5em;
			color: red;
			clear:none;
		}
		@-webkit-keyframes slideOut{
			0%{
				top:-4em;
			}
			30%{
				top:0;
			}
			70%{
				top:0;
			}
			100%{
				top:-4em;
			}
		}
		@keyframes slideOut{
			0%{
				top:-4em;
			}
			30%{
				top:0;
			}
			70%{
				top:0;
			}
			100%{
				top:-4em;
			}
		}

	</style>


</head>
<body>
<?php 

$dir = 'img/backgrounds';


if(!empty($_POST)){
	echo "<div id='message'> File created </div>";
}

?>


<div id="story-creator">
	<form method="POST">
		<div class="field">
			<label for="name">Filename</label>
			<input type="text" name='name' id="name" placeholder='In format 3_2, 5_4' required>
		</div>

		<div class="field">
			<label for="Title">Title</label>
			<input type="text" name="title" placeholder='Slide title, eg approaching dark castle...' id="title">
		</div>

		<div class="field">
			<label for="background">Background</label>
			<select name='background' required>
				<?php 
					if (is_dir($dir)){
					  	if ($dh = opendir($dir)){
					    	while (($file = readdir($dh)) !== false){
					    		echo "<option>{$file}</option>";

					    	}
					 	}
					} ?>
			</select>
		</div>

		<div class="field">
			<label for="text">Textblock</label>
			<textarea required name="text" id="text" cols="30" rows="10"></textarea>
		</div>

		<div class="field">
			<label>Options</label>
			
			<div class="option"><label for="option-1">Option 1</label><input type="text" required name="option-1" id="">
				<input type="text" name="option-1-link" id="" placeholder="Option filename, i.e 3_2">
			</div>
			<div class="option"><label for="option-2">Option 2</label><input type="text" name="option-2" id="">
				<input type="text" name="option-2-link" id="" placeholder="Option filename, i.e 3_2">
			</div>
			<div class="option"><label for="option-3">Option 3</label><input type="text" name="option-3" id="">
				<input type="text" name="option-3-link" id="" placeholder="Option filename, i.e 3_2">
			</div>
			<div class="option"><label for="option-4">Option 4</label><input type="text" name="option-4" id="">
				<input type="text" name="option-4-link" id="" placeholder="Option filename, i.e 3_2">
			</div>
			<div class="option"><label for="option-5">Option 5</label><input type="text" name="option-5" id="">
				<input type="text" name="option-5-link" id="" placeholder="Option filename, i.e 3_2">
			</div>
			<div class="option"><label for="option-6">Option 6</label><input type="text" name="option-6" id="">
				<input type="text" name="option-6-link" id="" placeholder="Option filename, i.e 3_2">
			</div>
			<div class="option"><label for="option-7">Option 7</label><input type="text" name="option-7" id="">
				<input type="text" name="option-7-link" id="" placeholder="Option filename, i.e 3_2">
			</div>
			
			<div class="field specials">
				<div class="field"><label for="special-1">Special 1</label><input type="text" name="special-1" id="" placeholder="Special vars, i.e mindcontrol, alien attack"></div>
				<div class="field"><label for="special-2">Special 2</label><input type="text" name="special-2" id="" placeholder="Special vars, i.e mindcontrol, alien attack"></div>
				<div class="field"><label for="special-3">Special 3</label><input type="text" name="special-3" id="" placeholder="Special vars, i.e mindcontrol, alien attack"></div>
				<div class="field"><label for="special-4">Special 4</label><input type="text" name="special-4" id="" placeholder="Special vars, i.e mindcontrol, alien attack"></div>
			</div>


		</div>

		<div class="field">
			<label for="item">Item Id</label>
			<input type="text" name="item" id='item' placeholder='Database ID'>
		</div>
			
		<div class="field"><input type="submit" value="Create"></div>
		
	</form>
</div>


</body>
</html>


<?php if(!empty($_POST)){

	function post($name){
		// Because I'm lazy
		if(is_string($name)){
			return $_POST[$name];
		}
		return false;
	}


	$data = array();

	$data['title'] 		= post('title');
	$data['text']		= post('text');
	$data['bg'] 		= post('background');
	$data['special']	= post('special');
	$data['item_id'] 	= post('item');

	for($i = 0; $i < 7; $i++){
		if(isset($_POST["option-".$i]) && !empty(post("option-".$i))  ){
			$data['options'][] 	= post('option-'.$i);
			$data['links'][] 	= post('option-'.$i."-link");
		}
	}

	for($i = 0; $i < 5; $i++ ){
		if(isset($_POST["special-".$i]) && !empty(post("special-".$i))){
			$data['specials'][] = post('special-'.$i);
 		}
	}

	$json = json_encode($data);

	$fp = fopen('story/'. $_POST['name'] . ".json", 'w');
	fwrite($fp, $json);
	fclose($fp);

}