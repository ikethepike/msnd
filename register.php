<?php include 'inc/head.php'; ?>

</head>
<body>

<?php include 'inc/header.php'; ?>

<main role="main">
	<div id="register-page-wrapper">
		<div id="register-block">
			<header id="register-header">
				<h3>Register</h3>
				<span class="description"></span>
			</header>

			<div id="register-form-wrapper">
				<?php include 'modules/register.php'; ?>
			</div>
		</div>

		<div id="register-canvas">
			<video autoplay poster='img/waving_trees.jpg' loop id="bg-vid">
			    <source src="img/waving_trees.mp4" type="video/mp4">
			</video>
		</div>



	</div>
</main>



<?php include 'inc/footer.php'; ?>