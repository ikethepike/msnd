<?php 

require_once "../core/init.php";

$DB = DB::getInstance();


// Parse file
$data = file_get_contents( "php://input" ); 
$data = (array) json_decode( $data );


$DB->get($data['table'], array($data['where'][0], $data['where'][1], $data['where'][2]));



die(json_encode($DB->first()));