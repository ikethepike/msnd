var handlers    = './handlers',
    gamePath    = '',
    first       = true,
    sessionId   = null;

document.addEventListener('DOMContentLoaded', function(){
    main();
});

        // Vars
        var hasSeed     = false,
            hasVial     = false,
            hasFlower   = false,
            hasStone    = false,
            hasSeed     = false,
            hasLeaf     = false;

        var multipleItems = false;





function main(){
    var options     = document.getElementsByClassName('option'),
        mainBox     = id('main-box'),
        textWrapper = id('ajax-text'),
        choices     = id('ajax-choices'),
        bgTarget    = id('bg-target'),
        title       = id('ajax-title'),
        modal       = id('modal'),
        modalTitle  = id('item-title'),
        modalImage  = id('item-image'),
        closeModal  = id('close-button'),
        invToggle   = id('inventory-toggle'),
        invWrapper  = id('inventory-wrapper'),
        invList     = id('inventory-list'),
        pageDim     = id('page-dim'),
        diceWrapper  = id('dice-wrapper'),
        dice        = id('dice'),
        currentBg   = null;

       // Reset dice and hide
        diceWrapper.className = 'dice-hidden';


        // Close modal
        closeModal.addEventListener('click', function(){
            closePopup();
        });

        dice.addEventListener('click', function(){
            rollDice();
        });

        invToggle.addEventListener('click', function(){
            toggleInventory();
        });

        function rollDice(){
            var roll = Math.floor(Math.random() * (6 - 1 + 1)) + 1;
            dice.innerHTML = "<span>" + roll + "</span>"; 
            return roll;
        }



    for(var i = 0; i < options.length; i++){
        
        // Move function
        options[i].addEventListener('click', function(e){
            e.preventDefault();

            var ajaxLink = this.getAttribute('href'); 

            if(ajaxLink.includes('end')){

                location.replace('./gameover?id=' + ajaxLink);

            } else {
                getJson('story/' + ajaxLink + ".json", function(data){
                    textWrapper.innerHTML     = data.text;

                    title.innerHTML = data.title;

                    if(data.bg !== currentBg){
                        bgTarget.style.backgroundImage = "url('img/backgrounds/" + data.bg + "')";
                        currentBg = data.bg;
                    }

                    choices.innerHTML = "";

                    // Add to vars

                    for(var i = 0; i < data.item_id.length; i++){
                        switch(data.item_id[i]){
                            // Seeds
                            case "13":
                                hasSeed = true;
                                break;
                            // Vial
                            case "12":
                                hasVial = true;
                                break;
                            // Flowers
                            case "17":
                                hasFlowers = true;
                                break;
                            // Stone
                            case "11":
                                hasStone = true
                                break;
                            // Leaf
                            case "14":
                                hasLeaf = true
                                break;
                        }
                    }


                    if(data.special !== null){
                       if(data.special === Object || Array.isArray(data.special)){
                            for(var i = 0; i < data.special.length; i++){
                                special(data.special[i]);
                            }
                       } else {
                        special(data.special);
                       }
                        

                    function special(special){
                        switch(special){
                            case "loseVial":
                                document.getElementById('item-12').remove();
                                break;
                            // DICE
                            case "dice":
                                console.log('dice');
                                break;
                        }
                    }

                } // DATA NULL CHECK

                    if(typeof data.item_id === Object || Array.isArray(data.item_id) ){
                        multipleItems = true;
                        for(var i = 0; i < data.item_id.length; i++){
                                getData('items', ['id', '=', data.item_id[i]], function(completed){
                                });
                        }
                    } else {
                        getData('items', ['id', '=', data.item_id], function(completed){

                        });
                    }
                    for(var i = 0; i < data.options.length; i++){
                        var length = '';
                        if(data.options[i].length > 60){
                            length = ' long';
                        }
                        choices.innerHTML += '<a class="option button-wrapper" href="' + data.links[i] +  '"><span class="button diamond' + length + '">' + data.options[i] + "<span></a>";
                    }
                    main();
                });

                // Fade effect
                fadeIn(mainBox, 1500);

                // Write path tp DB
                gamePath = gamePath + ajaxLink;

                var data = {
                    'contents' : {
                        'path'  : gamePath
                    },
                }

                if(first){
                    first = false;
                    insert('sessions', data);
                } else {
                    updateData('sessions', ['id', '=', sessionId] , data);
                }
                
            }

        });
    } // End of loop

    // Player functions
    var invOpen = false;
    function toggleInventory(){
        if(!invOpen){
            invOpen = true;
            invWrapper.className = 'inventory-visible';
        } else {
            invWrapper.className = 'inventory-hidden';
            invOpen = false;
        }
    }

    function closePopup(){
        modal.className = 'hidden-modal';
        pageDim.className = 'page-dim-inactive';
    }

} // End of Main



function id(name){
	if(typeof name === 'string'){
		return document.getElementById(name);
	}
	return false;
}

function fadeIn(el, time, callback) {
  el.style.opacity = 0;

  var last = +new Date();
  var tick = function() {
    el.style.opacity = +el.style.opacity + (new Date() - last) / time;
    last = +new Date();

    if (+el.style.opacity < 1) {
      (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
    }
  };

  tick();
}

function setLocal(key, value){
    if (typeof(Storage) !== "undefined") {
        localStorage.setItem(key, value);
        return true;
    } else {
        return false;
    }
}

function getLocal(key){
    return localStorage.getItem(key);
}

// Ajax load function
// Syntax example
// load('textfile.txt', function(xhr){
// 	document.getElementById('ajax-zone').innerHTML = xhr.responseText;
// });

function load(url, callback) {
    var xhr;

    if(typeof XMLHttpRequest !== 'undefined') xhr = new XMLHttpRequest();
    else {
        var versions = ["MSXML2.XmlHttp.5.0", 
                "MSXML2.XmlHttp.4.0",
                "MSXML2.XmlHttp.3.0", 
                "MSXML2.XmlHttp.2.0",
                "Microsoft.XmlHttp"]

        for(var i = 0, len = versions.length; i < len; i++) {
        try {
            xhr = new ActiveXObject(versions[i]);
            break;
        }
            catch(e){}
        } 
    }
        
    xhr.onreadystatechange = ensureReadiness;
        
    function ensureReadiness() {
        if(xhr.readyState < 4) {
            return;
        }
            
        if(xhr.status !== 200) {
            return;
        }


        if(xhr.readyState === 4) {
            callback(xhr);
        }           
    }
        
    xhr.open('GET', url, true);
    xhr.send('');
}

var handlers    = "./handlers/",
    xhttp       = new XMLHttpRequest(),
    pingback    = null;



function insert(table, data){

    if(typeof data !== 'string'){
        data['table']       = table;
        post( handlers + "insert.php", data, function(response){
            sessionId = response;
        });
    }
    return false;
}


var newInventory = true; 
// add to inventory function
function addInventory(item){
    var modalImage  = id('item-image'),
        modalTitle  = id('item-title'),
        pageDim     = id('page-dim'),
        invList     = id('inventory-list'),
        invWrapper  = id('inventory-wrapper');


    if(newInventory){
        invWrapper.className = 'inventory-pulse inventory-hidden';
        newInventory = false;
    }

    if(multipleItems){
        modalTitle.innerHTML = "You gained ";
        for(var i = 0; i < inventory.length; i++){
            modalTitle.innerHTML += inventory[i].name + ", ";
        }
    } else {
        modalImage.style.backgroundImage    = "url(img/inventory/" + item.image + ")"; 
        modalTitle.innerHTML                = item.name;
    }
    modal.className                     = 'visible-modal';
    pageDim.className                   = 'page-dim-active';
    
    invList.innerHTML += "<li id='item-" + item.id +  "'> <img src='img/inventory/" + item.image + "'> <span>" + item.name + "<span> </li>";

}

var inventory = [];
function getData(table, where, callback){

        if(typeof where !== 'string'){


            var data = {};

            data['table'] = table;
            data['where'] = where;

            post(handlers + "get.php", data, function(response){
                var item    = JSON.parse(response);
                inventory.push(item);
                setLocal('inventory', item);
                addInventory(item);
                callback(item);
            });

            
        }

}

function updateData(table, where, data){
    if(typeof data !== 'string'){
        data['where']       = where;
        data['table']       = table;
        data['contents']    = data.contents;

        update(data, function(response){
            // Success state
        });
    }
}

function update(data, callback){
    if(typeof data === 'string'){
        return false;
    }

    var data = JSON.stringify(data);
    
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            callback(xhttp.responseText);
        }
    };
        

    xhttp.open("POST", handlers + "update.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(data);   

}

function erase(table, where, callback){
    // var where in style array('id', '=', 2);

    if(typeof where !== array){

        data['table']   = table;
        data['where']   = where;


        post(handlers + "delete.php", data, function(){
            // Success handler
        });


    }
    return false;

}

function post(destination, data, callback){

    if(typeof data === 'string'){
        return false;
    }

    var data = JSON.stringify(data);


    xhttp.onreadystatechange = function() {

        if (xhttp.readyState == 4 && xhttp.status == 200) {
            callback(xhttp.responseText);
        }
    };
        

    xhttp.open("POST", destination, false);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(data);
}

function getJson(url, callback){

    var request = new XMLHttpRequest();
    request.open('GET', url, true);

    request.onload = function() {
      if (request.status >= 200 && request.status < 400) {

        var data = JSON.parse(request.responseText);

        callback(data);

        return true;
      } else {
        return false;

      }
    };

    request.onerror = function() {
        return false;
    };

    request.send();
    
}



function showDice(){
    var diceWrapper = id('dice-wrapper');
    diceWrapper.className ='dice-visible';
}
