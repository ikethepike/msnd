var handlers 	= "./handlers/",
	xhttp 		= new XMLHttpRequest(),
	pingback 	= null;



function insert(table, data){

	if(typeof data !== 'string'){
		data['table'] = table;
		post( handlers + "insert.php", data, function(response){
			// Success handler
		});
	}
	return false;
}

function updateData(table, id, data){
	if(typeof data !== 'string'){
		data['id'] 		= id;
		data['table'] 	= table;

		update(id, data, function(response){
			// Success state
		});
	}
}

function erase(table, where, callback){
	// var where in style array('id', '=', 2);

	if(typeof where !== array){

		data['table']	= table;
		data['where']	= where;


		post(handlers + "delete.php", data, function(){
			// Success handler
		});


	}
	return false;

}

function update(data, callback){
	if(typeof data === 'string'){
		return false;
	}

	var data = JSON.stringify(data);
	
	xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
			callback(xhttp.responseText);
        }
    };
		

	xhttp.open("POST", handlers + "update.php", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(data);	

}

function post(destination, data, callback){

	if(typeof data === 'string'){
		return false;
	}

	var data = JSON.stringify(data);


	xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
			callback(xhttp.responseText);
        }
    };
		

	xhttp.open("POST", destination, true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(data);
	
}

function getJson(url, callback){

	var request = new XMLHttpRequest();
	request.open('GET', url, true);

	request.onload = function() {
	  if (request.status >= 200 && request.status < 400) {

	    var data = JSON.parse(request.responseText);

	    callback(data);

	    return true;
	  } else {
	  	return false;

	  }
	};

	request.onerror = function() {
		return false;
	};

	request.send();
	
}
