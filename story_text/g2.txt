You chose to follow through with the King's order and try and get to your destination as soon as possible. If it means risking your life through the swamp - so be it.

Far distant from civilization, the Swamp of Sorrows is a stagnant marshland of sucking bogs and weeping trees. The ocean seeps into the fen, making the water brackish and the ground treacherous, perfect for minotaurs,  murlocs and even giants as they have always sought for solitude. 

There is but one safe path through the marshes. Fail in following it and join the hundreds of lost souls, trapped within the deceitful wasteland, trying to lour travelers to their death so they can join the undead horde.

[[Continue through the swamp]]  //h2