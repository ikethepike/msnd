You use your power over the forest to order the trees to bind the two lovers until your King arrives. The trees obay and close in on the two men. They are unable to fight the ancient foes. They are trapped by the trees and cannot move, but then they start screaming. You see the trees are continuing to squeeze the two men despite their cries for help. You order your subjects to stand down, but it is too late - they have squished the two men and their hearts beat no more.

Your king arrives furious of your doing and turns you into a caterpillar. You lose all memory of your life and are soon eaten by a crow.

// go togame over screen