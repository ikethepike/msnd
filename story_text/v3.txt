You return to your King and try to appear careless. You tell him of your success in retreiving the flower, but never mention the gruesome act of killing Dimitrius. The King is satisfied. He invites you to join him and observe as he uses the flower to make Titania fall in love with a donkey. This should be amusing!

[[Accept the King's invitation]] //y10
<<If used stone 2 times - no autocomplete [[Decline the King's invitation]]>> //y11
<<If used rock 1 time - no autocomplete [[Let the King go alone]]>> //y12