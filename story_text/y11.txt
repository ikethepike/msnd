As you are about to decline the King's invitation, you hear a now familiar voice in your head. It appears more powerful and urgent than before:"Quid pro Quo. Percutiam regem!" The bloody rock you left at Lysander's corpse once more appears in your hand. You look at Oberon, let out a monstrous shout and jab the stone shard in your King's neck. You hold his mouth shut to avoid attracting attention. Within seconds, you feel that life has left his body. "Quid pro Quo. Regis est mortuus," you whisper to the weapon. At that very moment you regain control of your body. 

You can do nothing but cry. 
"What is this cursed rock? Why did I have to use it's dark magic? I should have known there is a price to pay!"

Alas, what is done, is done. 

<<If used rock 2 times show (no autocomplete) [[Continue]]>>  //y1
<<If used rock 3 times show (no autocomplete) [[Forward]]>>  //y2