As the four wake up, they have a feeling of peace and love. All is the way it should be and destiny has won. Helena has gotten the man she had always wanted and Dimitrius - the women he never knew he loved and Lysander and Hermia can finally be together.
However, in the distance they see a search party. It is The duke of Athens himself! He rides up to them and just as he is about to sentence them to death, he sees their love and judges it to be pure. He allows the lovers be together and arranges a group wedding. 

[[See what's happened in the fairy realm]]  //62