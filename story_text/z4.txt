You decide to leave the King to his evil games and head home. You have had a very busy night and are in need of rest. 

You wake up late next morning and hear of the events that had taken place during your slumber. The king had waited until Titania and the donkey had finished their perverse act and then removed the spell and confronted the two. Titania, disgusted with herself and ashamed, gave up the changeling to Oberon without resistance. In return, Oberon agreed not to disclose her endevours with the donkey. The actor was turned back to his human form after he had played his part.

Dimitrius and Helena returned to Athens ecstatic of their new-found love and are planning to get married today so not to waste any time.

As for Hermia, her body was found next to the mutilated body of Lysander.She was well educated in both the remedies and poisons offered by the woods. She used it to find the blue flower of aconite and poisoned herself next to the corpse of Lysander. She had ended her life as she could not imagine spending it without him. 

You do not know what to think of yourself. You have comitted a terrible crime and know you will be punished by the Gods, but try hard to focus on the good. You have been a faithful servant to your King and as such are highly respected. This is why you decide not to think of the punishments due after your time has passed as your life is to be long and one you will try and enjoy to the fullest despite your servitude.

//go to gameover screen