As you are thinking of whether to put the couple to sleep and hear a voice in your head: "Quid pro quo. Neco!" You lose all control over your body. You take the stone given to you by Nas'Tarath, jump on top of Dimitrius and start smashing his skull with it. You hit him once, twice, three times and his head loses all the shape it once had. You say: "Quid pro quo. Ille est mortus."

You then snap out of it and see what you have done. Hermia starts screaming hysterically. You know not what to do and run away from the scene of the crime and toss the rock away, hoping to never see it again.

Shattered, you think what to do now and decide that reviving the love of Oberon and Titania will make up for the gruesome act. What is the life of one mortal... Right?

[[Try to save the love of Titania and Oberon]]  //v1
[[Think no more of this and return the flower to your King]]  //v3