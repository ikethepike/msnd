You decide to take the crystal as it may help you in your quest - you never know! The moment you get close to the crystal, you feel your feet sinking into the ground. You struggle to get free, but it is futile. The swamp slowly engulfs your feet, then legs, then torso, but the ground hardens before the marsh is able to swallow your head. You do your best to get free, but all is in vein. You cannot move and there is no hope for escape. You stay there for a day, a week, a month until you finaly starve.

//go togame over screen
