You follow Dimitrius and Lysander to a small yet clear area in the woods. Both of them draw their swords and prepare for a fight. It is clear that this will be a fight to the death. You panic and try to come up with a plan to stop them. What do you do? 

[[Replace the swords with flowers]]  //22

[[Hit both of them in the head and hope they fall unconscious]]  //23

[[Create a fog to cloud their vision]]  //24

[[Order trees to bind the two men until your king arrives]]  //21