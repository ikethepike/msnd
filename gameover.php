<?php 

	require_once('core/init.php');

	$DB = DB::getInstance();

	$session_id = $_SESSION['session_id'];

	$DB->get('sessions', array('id', '=', $session_id ));

	$result = $DB->first();

	$DB->get('sessions', array('path', '=', $result->path));
	$count = $DB->count();

	$percent = round( ($count / $DB->tableCount('sessions') ) * 100 );


	if($_GET['id'] !== null){
		$ending = $_GET['id'];
		$string = file_get_contents("story/" . $ending. ".json");
		$end = json_decode($string, true); 
	}


?>
<!DOCTYPE html>
<html>
<head>
	<title>Game Over</title>
	<link rel="stylesheet" type="text/css" href="css/gameover.css">
</head>
<body>

	<div id="gameover-box">
		<h1> <?php echo (empty($end['title'])) ? "Your quest is at an end." : $end['title']; ?> </h1>

		<span id='choices'> Your choices were the same as <span id='choice-percentage'> <?php echo $percent; ?>%</span> of players </span>
		
		<span id="epilogue">
			<?php if(!empty($end['epilogue'])){
					echo $end['epilogue'];
				} ?>
		</span>
		
		<span id="reference">
			<?php if(!empty($end['shakespeare'])){
				echo $end['shakespeare']; 
				} ?>
		</span>


		<a href="http://goodfellow.wearepanopticon.com" id="replay">Replay?</a>
	</div>

<div id="fade"></div>

<div id="gameover-canvas">
	<video autoplay poster='img/waving_trees.jpg' loop id="bg-vid">
	    <source src="img/waving_trees.mp4" type="video/mp4">
	</video>
</div>


</body>
</html>