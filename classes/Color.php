<?php 

class Color{

	// Generate random hex color
	public function random(){
		$rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
	    return $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
	}


	public function hex2rgb($hex) {
	$hex = str_replace("#", "", $hex);

	if(strlen($hex) == 3) {
	   $r = hexdec(substr($hex,0,1).substr($hex,0,1));
	   $g = hexdec(substr($hex,1,1).substr($hex,1,1));
	   $b = hexdec(substr($hex,2,1).substr($hex,2,1));
	} else {
	   $r = hexdec(substr($hex,0,2));
	   $g = hexdec(substr($hex,2,2));
	   $b = hexdec(substr($hex,4,2));
	}
	$rgb = array($r, $g, $b);

	return $rgb; 
	}

	public function darken(array $rgb, $decimal) {

	foreach($rgb as $color) {
	 $output[] = round($color * $decimal);
	}

	return "rgb({$output[0]}, {$output[1]}, {$output[2]})";
	}

	public function lighten(array $rgb, $decimal) {

	foreach($rgb as $color) {
	$output[] = round($color / $decimal);
	}

	return "rgb({$output[0]}, {$output[1]}, {$output[2]})";
	}

	public function rgb2hsl(array $rgb) {

	$r = ($rgb[0] / 255);
	$g = ($rgb[1] / 255);
	$b = ($rgb[2] / 255);

	$min    = min($r, $g, $b);
	$max    = max($r, $g, $b);
	$delta  = $max - $min; 
	$luma   = ($max + $min) / 2;

	if($delta == 0) {
	$hue = 0;
	$sat = 0;
	} else {

	($luma < 0.5) ? $sat = $delta / ($max + $min) : $sat = $delta / (2 - $max - $min);

	$delta_r = ( ( ( $max - $r) / 6) + ($max / 2)  ) / $delta;
	$delta_g = ( ( ( $max - $g) / 6) + ($max / 2)  ) / $delta;
	$delta_b = ( ( ( $max - $b) / 6) + ($max / 2)  ) / $delta;

	if($r == $max){
	  $hue = $delta_b - $delta_g;
	} else if($g == $max){
	  $hue = (1 / 3) + $delta_r - $delta_b;
	} else if($b == $max){
	  $hue = (2 / 3) + $delta_g - $delta_r;
	}

	if($hue < 0){
	  $hue += 1;
	}

	if($hue > 1){
	  $hue -= 1;
	}

	}

	$hsl = array($hue, $sat, $luma); 

	return $hsl;

	}

	public function complement($color){

	if(is_array($color)){
	$hsl = rgb2hsl($rgb);
	} else {
	$hsl = rgb2hsl(hex2rgb($color));
	}

	$h = $hsl[0];
	$s = $hsl[1];
	$l = $hsl[2];

	$h2 = $h + 0.5;

	if ($h2 > 1){
	 $h2 -= 1;
	}

	if ($s == 0){
	          $r = $l * 255;
	          $g = $l * 255;
	          $b = $l * 255;
	  }
	  else
	  {
	      if ($l < 0.5) {
	          $var_2 = $l * (1 + $s);
	      }
	      else
	      {
	          $var_2 = ($l + $s) - ($s * $l);
	      }

	      $var_1 = 2 * $l - $var_2;
	      $r = 255 * hue2rgb($var_1,$var_2,$h2 + (1 / 3));
	      $g = 255 * hue2rgb($var_1,$var_2,$h2);
	      $b = 255 * hue2rgb($var_1,$var_2,$h2 - (1 / 3));

	  }

	$rhex = sprintf("%02X",round($r));
	$ghex = sprintf("%02X",round($g));
	$bhex = sprintf("%02X",round($b));

	return $rgbhex = $rhex.$ghex.$bhex;

	}


	public function hue2rgb($v1,$v2,$vh) {
	if ($vh < 0){
	  $vh += 1;
	}

	if ($vh > 1){
	  $vh -= 1;
	}

	if ((6 * $vh) < 1){
	  return ($v1 + ($v2 - $v1) * 6 * $vh);
	}

	if ((2 * $vh) < 1){
	  return ($v2);
	}

	if ((3 * $vh) < 2){
	  return ($v1 + ($v2 - $v1) * ((2 / 3 - $vh) * 6));
	}

		return ($v1);
	}


}