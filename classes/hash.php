<?php

class Hash {

	public function make($length = 62){
		return bin2hex(openssl_random_pseudo_bytes($length));
	}

	public function password($password){
		return password_hash($password, Config::get('passwords/standard'), array('cost' => Config::get('passwords/cost')));
	}

}