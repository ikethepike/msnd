<?php

class Embed {
	
	public static function file($page){
		if(file_exists($page)){
			include($page);
		} else {
			echo "Error embedding file";
			return false;
		}
	}

}