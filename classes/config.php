<?php

// Function for returning Config data from Init

class Config{
	public static function get($path = null){
		if($path) {
			$config = $GLOBALS['config'];

			// Explode the $path into array
			$path = explode('/', $path);

			foreach ($path as $part) {
				if(isset($config[$part])){
					$config = $config[$part];
				}
			} // End foreach

			return $config;
		}
		return false;
	}
}