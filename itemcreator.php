<?php 

	require_once "core/init.php";

	$db = DB::getInstance();


	if(!empty($_POST)){

		function post($name){
			// Because I'm lazy
			if(is_string($name)){
				return $_POST[$name];
			}
			return false;
		}


		$data = array();

		$data['name'] 		= post('name');
		$data['image'] 		= post('image');

		$db->insert('items', array(
			'name'	=> $data['name'],
			'image'	=> $data['image'],
		));

	}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Item creator</title>

	<style>
	*{
		-webkit-box-sizing: border-box;
		box-sizing: border-box;

	}
		#item-creator{
			width:100%;
			max-width:45em;
			margin:auto;
			position: relative;
		}
		.field{
			margin:1em 0;
			clear:both;
		}
		input, textarea, select{
			resize:none;
			width:100%;
			border:1px solid #252525;
			min-height:3.5em;
			border-bottom:2px solid #252525;
		}
		label{
			display:block;
			padding:.35em 0;
			font-weight:bold;
		}
		#message{
			position: absolute;
			left:0;
			top:-4em;
			z-index:100;
			width:100%;
			background: green;
			color: #fff;
			text-align: center;
			padding:.5em 0;
			-webkit-animation: slideOut 2s 1;
			-moz-animation: slideOut 2s 1;
			animation: slideOut 2s 1;
		}
		@-webkit-keyframes slideOut{
			0%{
				top:-4em;
			}
			30%{
				top:0;
			}
			70%{
				top:0;
			}
			100%{
				top:-4em;
			}
		}
		@keyframes slideOut{
			0%{
				top:-4em;
			}
			30%{
				top:0;
			}
			70%{
				top:0;
			}
			100%{
				top:-4em;
			}
		}
		
		.last-insert{
			text-align: center;
			color: #999;
		}
		.last-insert h1{
			font-size:100%;
			font-size:4em;
			color: #F62459;
			display: block;
			margin:0;
			padding: 0;
		}
	
	</style>


</head>
<body>
<?php 

$dir = 'img/inventory';


if(!empty($_POST)){
	echo "<div id='message'> {$_POST['name']} created </div>";
}

?>
		
		
<div id="item-creator">
	<form method='POST'>
	<div class="field last-insert">
		<span>Last item ID:</span>
		<h1><?php echo $db->id(); ?> </h1>
	</div>


	<div class="field">
		<label for="name">Name</label>
		<input type="text" name='name' id="name" required>
	</div>
	
	<div class="field">
		<label for="image">Background</label>
		<select name='image' required>
			<?php 
				if (is_dir($dir)){
				  	if ($dh = opendir($dir)){
				    	while (($file = readdir($dh)) !== false){
				    		echo "<option>{$file}</option>";

				    	}
				 	}
				} ?>
		</select>
	</div>

	<div class="field">
		<input type="submit" value="Create">
	</div>
	</form>
</div>


</body>
</html>


