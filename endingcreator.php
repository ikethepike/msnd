<?php 

	require_once "core/init.php";

	$db = DB::getInstance();


	if(!empty($_POST)){

		function post($name){
			// Because I'm lazy
			if(is_string($name)){
				return $_POST[$name];
			}
			return false;
		}


		$data = array();

		$data['title'] 			= post('title');
		$data['image'] 			= post('image');
		$data['epilogue']		= post('epilogue');
		$data['shakespeare']	= post('shakespeare');

		$json = json_encode($data);

		$fp = fopen('story/'. $_POST['fileName'] . ".json", 'w');
		fwrite($fp, $json);
		fclose($fp);

	}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Ending Creator</title>

	<style>
	*{
		-webkit-box-sizing: border-box;
		box-sizing: border-box;

	}
		#item-creator{
			width:100%;
			max-width:45em;
			margin:auto;
			position: relative;
		}
		.field{
			margin:1em 0;
			clear:both;
		}
		input, textarea, select{
			resize:none;
			width:100%;
			border:1px solid #252525;
			min-height:3.5em;
			border-bottom:2px solid #252525;
		}
		label{
			display:block;
			padding:.35em 0;
			font-weight:bold;
		}
		#message{
			position: absolute;
			left:0;
			top:-4em;
			z-index:100;
			width:100%;
			background: green;
			color: #fff;
			text-align: center;
			padding:.5em 0;
			-webkit-animation: slideOut 2s 1;
			-moz-animation: slideOut 2s 1;
			animation: slideOut 2s 1;
		}
		@-webkit-keyframes slideOut{
			0%{
				top:-4em;
			}
			30%{
				top:0;
			}
			70%{
				top:0;
			}
			100%{
				top:-4em;
			}
		}
		@keyframes slideOut{
			0%{
				top:-4em;
			}
			30%{
				top:0;
			}
			70%{
				top:0;
			}
			100%{
				top:-4em;
			}
		}
		
		.last-insert{
			text-align: center;
			color: #999;
		}
		.last-insert h1{
			font-size:100%;
			font-size:4em;
			color: #F62459;
			display: block;
			margin:0;
			padding: 0;
		}
	
	</style>


</head>
<body>
<?php 

$dir = 'img/endings';


if(!empty($_POST)){
	echo "<div id='message'> {$_POST['title']} created </div>";
}

?>
		
		
<div id="item-creator">
	<form method='POST'>

	<div class="field">
		<label for="name">Title</label>
		<input type="text" name='title' id="title" required>
	</div>

	<div class="field"><label for="fileName">Filename</label><input type="text" name="fileName" id="fileName"></div>
	
	<div class="field">
		<label for="image">Image</label>
		<select name='image' required>
			<?php 
				if (is_dir($dir)){
				  	if ($dh = opendir($dir)){
				    	while (($file = readdir($dh)) !== false){
				    		echo "<option>{$file}</option>";

				    	}
				 	}
				} ?>
		</select>
	</div>

	<div class="field"><label for="epilogue">Epilogue</label><textarea name="epilogue" id="epilogue" cols="30" rows="10"></textarea></div>
	
	<div class="field"><label for="shakespeare">Shakespeare Reference</label><input type="text" name="shakespeare" id="shakespeare"></div>


	<div class="field">
		<input type="submit" value="End me">
	</div>
	</form>
</div>


</body>
</html>


