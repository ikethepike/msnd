<?php
	// General Site Vars and functions
	require "core/init.php";

?>

<!DOCTYPE html>
<html lang='en'>
<head>

	<!-- Single page site 
	<title> <?php // echo $page_title; ?> </title>
	-->

	<!-- Multi page site -->
	<title> <?php echo $site_title; if(isset($subpage_title)){ echo " | " . $subpage_title; } ?> </title>

	
	<link rel="stylesheet" type="text/css" href="css/base.css">
	<script type="text/javascript" src='js/min/scripts.js'></script>

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name='description' content="<?php echo $GLOBALS['site_description']; ?>">
	

	<!-- Fonts Playfair -->
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">


	