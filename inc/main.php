<main role="main">
	<div class="body-wrapper" id='bg-target' style='background: url(img/backgrounds/dimitrius_helena.jpg) no-repeat center;'>
		<div id="bg-fade"></div>
		
		<div id='main-box'>
			<div> <h1 id="ajax-title">And so, it begins...</h1></div>
			<div id="ajax-text" class='hidden'>
				A great wedding is being planned in Athens. Hermia is to be wed to Demitrius, who loves her with all his heart and soul. Her heart, however, belongs to another man, Lysander, who has vowed eternal love to her by God himself. All this is slowly breaking the spirit of Helena, who would wish nothing more but to be with Demitrius. And yet, the law is clear. Hermia has to either obey her father and marry the noble Athenian or face either death or a life as a nun, forever worshiping Artemis.

			</div>
			<div id="ajax-choices" class='hidden'>
				<a class='button-wrapper option' href="a1"><span class="button diamond">Next</span></a>
			</div>

		</div>


		<div id="modal" class="hidden-modal">
			<div id="item-image"></div>
			<span id="item-title"></span>
			<span class="instruction">
				You can access your inventory anytime by pressing the tab on the side. 
			</span>
			
			<a role='button' id="close-button">Close</a>
		</div>

		<div id="page-dim" class='page-dim-inactive'></div>
		


		<?php include 'inventory.php'; ?>

		<?php include 'dice.php'; ?>



	</div>
</main>


